name             'gitlab-grafana'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact@gitlab.com'
license          'MIT'
description      'Cookbook for GitLab Grafana'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.0.10'

depends 'cron'
depends 'gitlab_secrets'
