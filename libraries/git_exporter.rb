require 'fileutils'

module GrafanaCookbook
  class GitExporter
    def initialize(repo_path, url, api_key, node)
      @repo_path = repo_path
      @node = node
      @url, @api_key = url, api_key
    end

    def execute
      FileUtils.rm_rf(dashboards_path) if Dir.exist?(dashboards_path)
      FileUtils.mkdir(dashboards_path)

      grafana_dashboards.each do |dashboard|
        json = get_dashboard(dashboard)

        File.open(dashboard_path(json), 'w') do |file|
          file.write(json)
        end
      end

      Dir.chdir(@repo_path) do
        `git config user.name "Automated export"`
        `git config user.email "#{@node['grafana']['public_email']}"`
        `git add . && git commit -m "Export of #{Time.now}"`
        `git checkout master`
        `git merge deploy`
        `git push -u origin master`
      end
    end

    def request_headers
      {
        'Authorization' => "Bearer #{@api_key}",
        'Accept' => 'application/json',
        'Content-Type' => 'application/json'
      }
    end

    private

    def grafana_dashboards
      JSON.parse(http_client.get("/api/search", 
                                 request_headers))
    end

    # Takes the dashboard hash from the dashboards method
    def get_dashboard(dashboard)
      json = http_client.get("/api/dashboards/#{dashboard['uri']}", request_headers)
      json.sub!("\"id\":#{dashboard['id']}",'"id":null')
      json.sub!(/\"updatedBy\":\s*\"\S+?\"/,"\"updatedBy\":\"#{@node['grafana']['public_email']}\"")
      json.sub!(/\"createdBy\":\s*\"\S+?\"/,"\"createdBy\":\"#{@node['grafana']['public_email']}\"")
      json.gsub(/}\n\z/, ",\"overwrite\":true}\n")
    end

    def dashboard_path(json)
      slug = JSON.parse(json)['meta']['slug']

      File.join(dashboards_path, slug + '.json')
    end

    def http_client
      @http_client ||= Chef::HTTP.new(@url)
    end

    def dashboards_path
      File.join(@repo_path, 'dashboards')
    end
  end
end
