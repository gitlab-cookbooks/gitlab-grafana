
default['grafana']['chef_vault'] = 'public-grafana'
default['grafana']['chef_vault_item'] = '_default'

default['grafana']['package']['version'] = '7.2.0'
default['grafana']['package']['repo'] = 'https://packages.grafana.com/oss/deb'
default['grafana']['package']['distribution'] = 'stable'
default['grafana']['package']['components'] = ['main']
default['grafana']['package']['key'] = 'https://packages.grafana.com/gpg.key'
default['grafana']['package']['apt_rebuild'] = true
default['grafana']['package']['trusted'] = false

default['grafana']['user'] = 'grafana'
default['grafana']['group'] = 'grafana'
default['grafana']['home'] = '/usr/share/grafana'
default['grafana']['conf_dir'] = '/etc/grafana'
default['grafana']['data_dir'] = '/var/lib/grafana'
default['grafana']['log_dir'] = '/var/log/grafana'
default['grafana']['plugins_dir'] = '/var/lib/grafana/plugins'

default['grafana']['env_dir'] = '/etc/default'


default['grafana']['ini'][nil]['app_mode'] = 'production'

default['grafana']['ini']['database']['type'] = {
  comment: "Either mysql, postgres, sqlite3, it's your choice",
  disable: false,
  value: 'sqlite3'
}
default['grafana']['ini']['database']['host'] = '127.0.0.1:3306'
default['grafana']['ini']['database']['name'] = 'grafana'
default['grafana']['ini']['database']['user'] = 'root'
default['grafana']['ini']['database']['password'] = ''
default['grafana']['ini']['database']['ssl_mode'] = {
  comment: 'For "postgres" only, either "disable", "require" or "verify-full"',
  disable: true,
  value: 'disable'
}
default['grafana']['ini']['database']['path'] = {
  comment: 'For sqlite3 only, path relative to data_path setting',
  disable: false,
  value: 'grafana.db'
}

default['grafana']['ini']['auth.ldap']['enabled'] = {
  comment: '',
  disable: true,
  value: false
}

# security
default['grafana']['ini']['security']['disable_gravatar'] = 'true'

# server
default['grafana']['ini']['server']['protocol'] = 'http'
default['grafana']['ini']['server']['http_port'] = 3000
default['grafana']['ini']['server']['domain'] = 'localhost'
default['grafana']['ini']['server']['router_logging'] = 'true'

# session
default['grafana']['ini']['session']['provider'] = 'memcache'
default['grafana']['ini']['session']['provider_config'] = '127.0.0.1:11211'
default['grafana']['ini']['session']['session_life_time'] = '86400'

default['grafana']['synchronize'] = false

# Dashboard repo sync.
default['grafana']['git_http'] = "https://gitlab.com/gitlab-org/grafana-dashboards.git"
default['grafana']['branch'] = "master"
default['grafana']['repo_path'] = File.join(Chef::Config[:file_cache_path], 'grafana-dashboards')
default['grafana']['public_email'] = "ops-contact@gitlab.com"
