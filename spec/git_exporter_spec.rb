require 'spec_helper'
require 'webmock/rspec'
require_relative '../libraries/git_exporter.rb'

describe GrafanaCookbook::GitExporter do
  let!(:repo_path) { Dir.mktmpdir }
  let(:fixture_path) { File.join(File.dirname(__FILE__), 'fixtures') }

  after(:each)  do
    FileUtils.remove_entry_secure(repo_path)
  end

  describe '#execute' do
    subject { described_class.new(repo_path, 'http://url.tld', 'gitlab', {
      "grafana" => {
        "public_email" => "infra@gitlab.com",
        "search_endpoint" => "api/search",
        "dashboard_endpoint" => "api/dashboards",
      }
    }) }
    let(:search_response) { File.read(File.join(fixture_path, 'search.json')) }
    let(:api_v3_actions_response) { File.read(File.join(fixture_path, 'api-v3-actions-by-count.json')) }
    let(:andrew_test_daily_response) { File.read(File.join(fixture_path, 'andrew-test-daily-overview.json')) }

    before do
      stub_request(:get, "http://url.tld/api/search")
        .to_return(body: search_response)

      stub_request(:get, "http://url.tld/api/dashboards/db/api-v3-actions-by-count")
        .to_return(body: api_v3_actions_response)

      stub_request(:get, "http://url.tld/api/dashboards/db/andrew-test-daily-overview")
        .to_return(body: andrew_test_daily_response)

      subject.execute
    end

    context 'git folder preperation' do
      it 'creates the dashboard folder' do
        expect(Dir.entries(repo_path).sort).to eq(['.','..', 'dashboards'])
      end
    end

    context 'exporting json files' do
      let(:dashboards_path) { File.join(repo_path, 'dashboards') }

      let(:entries) do
        Dir.entries(dashboards_path).reject { |e| e.start_with?('.') }.sort
      end

      it 'created two dashboard files' do
        expect(entries).to include("andrew-test-daily-overview.json")
        expect(entries).to include("api-v3-actions-by-count.json")
      end

      it 'prepares the data for publication' do
        entries.each do |file|
          file = File.read(File.join(dashboards_path, file))

          expect(file).not_to match /\"id\":\d+/
          expect(file).to match /\"updatedBy\":\"infra@gitlab.com\"/
          expect(file).to match /\"createdBy\":\"infra@gitlab.com\"/
          expect(file).to match /,\"overwrite\":true}\n\z/
        end
      end
    end
  end
end
