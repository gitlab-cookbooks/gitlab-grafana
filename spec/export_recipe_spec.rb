require "spec_helper"
require 'chef-vault'
require "chefspec"

describe "gitlab-grafana::export_dashboards" do
  before do
    allow_any_instance_of(Chef::Recipe).to receive(:get_secrets)
      .with('fake_backend', 'fake_path', 'fake_key')
      .and_return(
        'id' => '_default',
        'grafana' => {
          'api_key' => 'my-api-key',
          'external_url' => 'http://monitor.gitlab.net',
          'private' => {
            'api_key' => 'my-api-key',
            'url' => 'http://performance.gitlab.net'
          },
          'public' => {
            'api_key' => 'my-api-key',
            'url' => 'http://performance.gitlab.com'
          },
          'upstream' => {
            'api_key' => 'my-api-key',
            'url' => 'http://performance.gitlab.net'
          },
          'git_username' => 'username',
          'git_password' => 'password'
        }
      )
    allow(Chef::DataBag)
      .to receive(:load).with('public-grafana').and_return('password_keys' => {})
  end

  context "execution" do
    let(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
        node.normal['grafana'] = {
          'repo_path' => 'grafana-dashboards',
          'secrets' => {
            'backend' => 'fake_backend',
            'path' => 'fake_path',
            'key' => 'fake_key',
          },
        }
      }.converge(described_recipe)
    end

    it 'syncs the git repo' do
      expect(chef_run).to sync_git('grafana-dashboards').with(
        repository: 'https://username:password@gitlab.com/gitlab-org/grafana-dashboards.git',
        revision: 'master',
      )
    end
  end
end
