require "spec_helper"
require 'chef-vault'
require "chefspec"

describe "gitlab-grafana::default" do

  before do
    allow(Chef::DataBag)
      .to receive(:load).with('public-grafana').and_return('password_keys' => {})
    allow(ChefVault::Item)
      .to receive(:load).with('public-grafana', '_default').and_return({
      'id'       => '_default',
      'grafana' => {
        'api_key' => 'my-api-key',
        'external_url' => 'http://monitor.gitlab.net',
        'upstream' => {
          'api_key' => 'my-api-key',
          'url' => 'http://performance.gitlab.net'
        }
      }
    })
  end


  context "default execution" do
    let(:chef_run) {
      ChefSpec::SoloRunner.new { |node|
      }.converge(described_recipe)
    }

    it 'installs apt-transport-https' do
      expect(chef_run).to install_package('apt-transport-https')
    end

    it 'installs memacached' do
      expect(chef_run).to install_package('memcached')
    end

    it 'creates default memcached settings' do
      expect(chef_run).to create_cookbook_file("/etc/memcached.conf").with(
        source: 'memcached.conf',
        owner: 'root',
        group: 'root',
        mode: '0644'
      )
    end

    it "installs grafana" do
      expect(chef_run).to install_apt_package('grafana').with(
        :options => ['-o', 'Dpkg::Options::=--force-confdef', '-o', 'Dpkg::Options::=--force-confold']
      )
    end

    it "enables grafana-server service" do
      expect(chef_run).to enable_service('grafana-server').with(
        :supports => {
          :start => true,
          :stop => true,
          :restart => true,
          :status => true,
          :reload => false
        }
      )
    end

    it "create grafana data dir" do
      expect(chef_run).to create_directory("/var/lib/grafana").with(
        owner: "grafana",
        group: "grafana",
        mode: "0755"
      )
    end

    it "create grafana plugins dir" do
      expect(chef_run).to create_directory("/var/lib/grafana/plugins").with(
        owner: "grafana",
        group: "grafana",
        mode: "0755"
      )
    end

    it "create grafana log dir" do
      expect(chef_run).to create_directory("/var/log/grafana").with(
        owner: "grafana",
        group: "grafana",
        mode: "0755"
      )
    end

    it "creates default grafana-server settings" do
      expect(chef_run).to create_template("/etc/default/grafana-server").with(
        source: 'grafana-env.erb',
        owner: "root",
        group: "root",
        mode: "0644"
      )
    end

    it "creates grafana.ini settings" do
      expect(chef_run).to create_template("/etc/grafana/grafana.ini").with(
        source: 'grafana.ini.erb',
        owner: "root",
        group: "root",
        mode: "0644"
      )
    end

  end
end
