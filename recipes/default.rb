#
# Cookbook Name:: gitlab-grafana
# Recipe:: default
# License:: MIT
#
# Copyright 2017, GitLab Inc.
#
#
package 'apt-transport-https'
package 'memcached'

execute 'restart-memcached' do
  command 'systemctl restart memcached'
  action :nothing
end

cookbook_file "/etc/memcached.conf" do
  source "memcached.conf"
  owner "root"
  group "root"
  mode "0644"
  notifies :run, 'execute[restart-memcached]'
end

apt_repository 'grafana' do
  uri node['grafana']['package']['repo']
  distribution node['grafana']['package']['distribution']
  components node['grafana']['package']['components']
  key node['grafana']['package']['key']
  cache_rebuild node['grafana']['package']['apt_rebuild']
  trusted node['grafana']['package']['trusted']
end

apt_package 'grafana' do
  version node['grafana']['package']['version']
  options '-o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold"'
end

g_service = service 'grafana-server' do
  supports start: true, stop: true, restart: true, status: true, reload: false
  action :enable
end

directory node['grafana']['data_dir'] do
  owner node['grafana']['user']
  group node['grafana']['group']
  mode '0755'
  action :create
end

directory node['grafana']['plugins_dir'] do
  owner node['grafana']['user']
  group node['grafana']['group']
  mode '0755'
  action :create
end

directory node['grafana']['log_dir'] do
  owner node['grafana']['user']
  group node['grafana']['group']
  mode '0755'
  action :create
end

g_default_template = template '/etc/default/grafana-server' do
  source 'grafana-env.erb'
  variables(
    grafana_user: node['grafana']['user'],
    grafana_group: node['grafana']['group'],
    grafana_home: node['grafana']['home'],
    log_dir: node['grafana']['log_dir'],
    data_dir: node['grafana']['data_dir'],
    conf_dir: node['grafana']['conf_dir'],
    plugins_dir: node['grafana']['plugins_dir']
  )
  owner 'root'
  group 'root'
  mode '0644'
end

ini = node['grafana']['ini'].dup
if node['grafana'].attribute?('secrets')
  ## Merge in secrets for the ini
  ## if it exists
  ini_secrets = get_secrets(node['grafana']['secrets']['backend'],
                            node['grafana']['secrets']['path'],
                            node['grafana']['secrets']['key'])
                .dig('grafana', 'ini')
  ini = ini.deep_merge(ini_secrets || {})
end
ini['paths'] ||= {}
ini['paths']['data'] = node['grafana']['data_dir']
ini['paths']['logs'] = node['grafana']['log_dir']
ini['paths']['plugins'] = node['grafana']['plugins_dir']

g_ini_template = template "#{node['grafana']['conf_dir']}/grafana.ini" do
  source 'grafana.ini.erb'
  variables ini: ini
  owner 'root'
  group 'root'
  mode '0644'
  sensitive true
end

ruby_block 'restart grafana immediately after config change' do
  block { g_service.run_action :restart }
  only_if do
    g_default_template.updated_by_last_action? ||
      g_ini_template.updated_by_last_action?
  end
end
