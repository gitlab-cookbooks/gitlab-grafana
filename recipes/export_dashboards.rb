require 'uri'

secrets = get_secrets(node['grafana']['secrets']['backend'],
                      node['grafana']['secrets']['path'],
                      node['grafana']['secrets']['key'])
 
git_http_url = URI.parse(node['grafana']['git_http'])
git_http_url.userinfo = "#{URI.encode(secrets['grafana']['git_username'])}:#{URI.encode(secrets['grafana']['git_password'])}"

git node['grafana']['repo_path'] do
  repository      git_http_url.to_s
  revision        node['grafana']['branch'] || 'master'
  action          :sync
end

ruby_block "export dashboards to #{node['grafana']['git_http']}" do
  block do
    private_secrets = secrets['grafana']['private']

    GrafanaCookbook::GitExporter.new(node['grafana']['repo_path'], 
                    private_secrets['url'],
                    private_secrets['api_key'],
                    node,
                    ).execute
  end
  action :run
end

package "ruby"

cookbook_file "/usr/local/sbin/sync_grafana_dashboards" do
  source "sync_grafana_dashboards.rb"
  owner "root"
  group "root"
  mode "0755"
end

sync_env = {
  "PRIVATE_API_KEY" => secrets['grafana']['private']['api_key'],
  "PRIVATE_URL" => secrets['grafana']['private']['url'],
  "PUBLIC_API_KEY" => secrets['grafana']['public']['api_key'],
  "PUBLIC_URL" => secrets['grafana']['public']['url'],
}

cron "sync-grafana-dashboards" do
  command "/usr/local/sbin/sync_grafana_dashboards"
  environment sync_env
  # Take care making this any more frequent; each run creates a new version of 
  # every dashboard (unavoidable currently; grafana does this, and cannot be stopped)
  # Grafana tries to delete older versions (more than 20), but only does this
  # every 10 minutes, and only 100 dashboards each time.  If we do the sync
  # too frequently, grafana cannot keep up and grafana.db grows indefinitely
  # At this writing: 222 dashboards means we cannot run more frequently than
  # every 30 minutes.  Once an hour is safe up to 600 dashboards.
  minute '0'  
end
